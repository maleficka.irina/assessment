const LoginPage = require('./login-page');
const InventoryPage = require('./inventory-page');

/**
 * 
 * @param  name {'login' | 'inventory'}
 * @returns {*}
 */
function pages(name) {
  const page = {
    login: new LoginPage(),
    inventory: new InventoryPage()
  }
  return page[name.toLowerCase()]
}

module.exports = {
  pages,
  LoginPage,
  InventoryPage
}