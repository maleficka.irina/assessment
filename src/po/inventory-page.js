// page open after login, inventory page
class InventoryPage {
  get pageHeader() {
    return $('.header_label .app_logo')
  }

  getHeaderText() {
    return this.pageHeader.getText();
  }
}

module.exports = InventoryPage;