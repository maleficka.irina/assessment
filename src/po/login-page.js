const { Key } = require('webdriverio');

class LoginPage {
  get username() {
    return $('[data-test="username"]');
  }

  get password() {
    return $('[data-test="password"]');
  }

  get errorMessage() {
    return $('[data-test="error"]');
  }

  get loginBtn() {
    return $('[data-test="login-button"]');
  }

  getUsernameValue() {
    return this.username.getValue();
  }

  getPasswordValue() {
    return this.password.getValue();
  }

  typeUsername(name) {
    this.username.setValue(name);
  }

  typePassword(pass) {
    this.password.setValue(pass);
  }

  /**
   * 
   * @param browser 
   * @param field {'username' | 'password'}
   */
  async clearField(field) {
    await field.click();
    await browser.action('key')
      .down(Key.Ctrl).down('a').perform();
    await browser.action('key')
      .down(Key.Delete).perform()
  }

  async clearPassword() {
    this.clearField(this.password);
  }
  async clearUsername() {
    this.clearField(this.username);
  }

  clickLoginBtn() {
    this.loginBtn.click()
  }
}

module.exports = LoginPage;