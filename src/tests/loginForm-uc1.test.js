const { pages } = require('../po/index')

const USERNAMES = 'standard_user'//, 'locked_out_user', 'problem_user', 'performance_glitch_user', 'error_user', 'visual_user'];
const PASSWORD = 'secret_sauce';

describe('Login Form @login', () => {
  beforeEach(async () => {
    await browser.url('/');
  })

  it('UC -1 - Test Login form with empty credentials: @uc1 @login', async () => {
    // Type any credentials.
    await pages('login').typeUsername(USERNAMES);
    await pages('login').typePassword(PASSWORD);
    await browser.pause(2000);
    // Clear the inputs.
    await pages('login').clearUsername();
    await pages('login').clearPassword();
    await browser.pause(2000);
    await pages('login').clickLoginBtn();
    // Check the error messages: 3.1 Username is required.
    await expect(pages('login').errorMessage).toHaveTextContaining('Username is required');
  })



})
