const { pages } = require('../po/index');
const { Key } = require('webdriverio');


const USERNAMES = 'standard_user'//, 'locked_out_user', 'problem_user', 'performance_glitch_user', 'error_user', 'visual_user'];
const PASSWORD = 'secret_sauce';

describe('Login Form @login', () => {
  beforeEach(async () => {
    await browser.url('/');
  })

  it('UC -2 - Test Login form with credentials by passing Username: @uc2 @login', async () => {
    // Type any credentials in username.
    // Enter password and Clear the input.
    await pages('login').typeUsername(USERNAMES);
    await pages('login').typePassword(PASSWORD);
    await browser.pause(2000);
    await pages('login').clearPassword();
    await browser.pause(3000);
    await pages('login').clickLoginBtn();
    // Check the error messages: 3.1 Password is required.
    await expect(pages('login').errorMessage).toHaveTextContaining('Password is required');
  })



})
