const { pages } = require('../po/index')

const USERNAMES = 'standard_user'//, 'locked_out_user', 'problem_user', 'performance_glitch_user', 'error_user', 'visual_user'];
const PASSWORD = 'secret_sauce';

describe('Login Form @login', () => {
  beforeEach(async () => {
    await browser.url('/');
  })

  it('UC -3 - Test Login form with credentials by passing Username & Password: @uc3 @login', async () => {
    // Type credentials in username which are under Accepted username are sections.
    await pages('login').typeUsername(USERNAMES);
    // Enter password as secret sauce.
    await pages('login').typePassword(PASSWORD);
    // Click on Login and validate the title “Swag Labs” in the dashboard.
    await browser.pause(2000);
    await pages('login').clickLoginBtn();

    await expect(browser).toHaveUrl('https://www.saucedemo.com/inventory.html');
    await expect(pages('inventory').pageHeader).toHaveText('Swag Labs')

  })

})
